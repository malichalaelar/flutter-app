import 'package:flutter/material.dart';

TextStyle mainHeader =
    TextStyle(fontSize: 24, color: Colors.green, fontWeight: FontWeight.w500);

TextStyle subTitle = TextStyle(fontSize: 14, color: Colors.brown[200]);
