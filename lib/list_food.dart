import 'package:flutter/material.dart';
import 'package:flutter_app/cart.dart';
import 'package:scoped_model/scoped_model.dart';
import 'main_style.dart';
import 'dart:ui';
import "package:flutter_app/cart_model.dart";

class ListFood extends StatelessWidget {
  List<Product> _products = [
    Product(
        id: 1,
        title: 'Burger',
        price: 14000,
        image:
            "https://cdn.pixabay.com/photo/2012/04/13/01/51/hamburger-31775__340.png",
        qty: 1),
    Product(
        id: 2,
        title: "Pizza",
        price: 10000,
        image:
            "https://www.pinclipart.com/picdir/big/343-3432287_italian-clipart-pizza-pizza-cartoon-transparent-background-png.png",
        qty: 1),
    Product(
        id: 3,
        title: "French fries",
        price: 9000,
        image:
            "https://images.vexels.com/media/users/3/149820/isolated/preview/73fb60c8f9821107d4f14d0c620919fe-desenho-de-personagem-de-caixa-de-batatas-fritas-by-vexels.png",
        qty: 1),
    Product(
        id: 4,
        title: "Donut",
        price: 8000,
        image: "https://webstockreview.net/images/donut-clipart-cute-18.png",
        qty: 1),
    Product(
        id: 5,
        title: "Coca Cola",
        price: 5000,
        image: "https://kookospizza.ca/wp-content/uploads/2018/10/can.jpg",
        qty: 1),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("List Food"),
        automaticallyImplyLeading: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add_shopping_cart),
              onPressed: () {
                {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Cart()),
                  );
                }
              })
        ],
      ),
      body: GridView.builder(
        padding: EdgeInsets.all(8.0),
        itemCount: _products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 8,
            crossAxisSpacing: 8,
            childAspectRatio: 0.8),
        itemBuilder: (context, index) {
          return ScopedModelDescendant<CartModel>(
              builder: (context, child, model) {
            return Card(
                child: Column(children: <Widget>[
              Image.network(
                _products[index].image,
                height: 120,
                width: 120,
              ),
              Text(
                _products[index].title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text("\Rp." + _products[index].price.toString()),
              FlatButton(
                  child: Text("Add"),
                  textColor: Colors.white,
                  color: Colors.green,
                  onPressed: () => model.addProduct(_products[index]))
            ]));
          });
        },
      ),
    );
  }
}
