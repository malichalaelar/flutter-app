import 'package:flutter/material.dart';
import 'package:flutter_app/list_food.dart';
import 'main_style.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Image.network(
                  'https://image.freepik.com/free-vector/delicious-fast-food-menu_24877-51680.jpg'),
              Padding(
                padding: EdgeInsets.only(top: 30.0),
              ),
              Text(
                "Selamat Datang",
                style: mainHeader,
              ),
              Padding(
                padding: EdgeInsets.only(top: 30.0),
              ),
              Text(
                "Masuk dan tambahkan makanan di keranjang",
                style: subTitle,
              ),
              Padding(
                padding: EdgeInsets.only(top: 30.0),
              ),
              FlatButton(
                textColor: Colors.white,
                color: Colors.green,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ListFood()),
                  );
                },
                child: Text("Masuk Disini"),
              )
            ],
          ),
        ),
      ),
    );
  }
}
