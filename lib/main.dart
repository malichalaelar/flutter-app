import 'package:flutter/material.dart';
import 'package:flutter_app/list_food.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_app/cart_model.dart';
import 'package:flutter_app/cart.dart';
import 'package:flutter_app/home.dart';

void main() => runApp(MyApp(
      model: CartModel(),
    ));

class MyApp extends StatelessWidget {
  final CartModel model;

  const MyApp({Key key, @required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ScopedModel<CartModel>(
      model: model,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Shopping Cart',
        home: Home(),
        routes: {'/cart': (context) => Cart()},
      ),
    );
  }
}
